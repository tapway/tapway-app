import React from 'react';
import {
  // Actions,
  Scene,
  Router,
  Drawer,
  // Modal,
} from 'react-native-router-flux';
// import { StackViewStyleInterpolator } from 'react-navigation-stack';
// import Trending from './screens/Trending';
// import ErrorModal from './components/modal/ErrorModal';
// import DrawerContent from './components/drawer/DrawerContent';

import IntroPage from './components/IntroPage';

import Login from './components/Login';
import ForgetPassword from './components/ForgetPassword';

import SignUp from './components/SignUp';
import VerifySignUp from './components/SignUp/VerifySignUp';

import HomePage from './components/HomePage';
import ProfileSetting from './components/ProfileSetting';
import OrderHistory from './components/OrderHistory';

import CheckPrice from './components/CheckPrice';
import CheckNearestDriver from './components/CheckNearestDriver';
import CheckDeliveryTime from './components/CheckDeliveryTime';

import DrawerContent from './components/DrawerContent';
import PackageDetails from './components/HomePage/PackageDetails';
import TrackOrder from './components/TrackOrder';
import FAQ from './components/FAQ';
import CustomerCare from './components/CustomerCare';
import Wallet from './components/Wallet';

// const transitionConfig = () => ({
//   screenInterpolator: StackViewStyleInterpolator.forFadeFromBottomAndroid,
// });

const Scenes = () => (
  <Router>
    {/* <Modal hideNavBar> */}
    <Scene key="root" hideNavBar>
      {/* <Scene key="loginSignUp" hideNavBar> */}
      <Scene key="IntroPage" component={IntroPage} hideNavBar />

      <Scene key="Login" component={Login} hideNavBar />
      <Scene key="ForgetPassword" component={ForgetPassword} hideNavBar />

      <Scene key="SignUp" component={SignUp} hideNavBar />
      <Scene key="VerifySignUp" component={VerifySignUp} hideNavBar />
      {/* </Scene> */}

      <Scene
        key="CheckNearestDriver"
        component={CheckNearestDriver}
        hideNavBar
      />
      <Scene key="CheckPrice" component={CheckPrice} hideNavBar />
      <Scene key="CheckDeliveryTime" component={CheckDeliveryTime} hideNavBar />

      <Drawer
        hideNavBar
        key="drawer"
        contentComponent={DrawerContent}
        drawerWidth={300}>
        <Scene key="Home" hideNavBar>
          <Scene key="HomePage" component={HomePage} hideNavBar initial />
          <Scene key="ProfileSetting" component={ProfileSetting} hideNavBar />
          <Scene key="OrderHistory" component={OrderHistory} hideNavBar />
          <Scene key="PackageDetails" component={PackageDetails} hideNavBar />
          <Scene key="TrackOrder" component={TrackOrder} hideNavBar />
          <Scene key="FAQ" component={FAQ} hideNavBar />
          <Scene key="CustomerCare" component={CustomerCare} hideNavBar />
          <Scene key="Wallet" component={Wallet} hideNavBar />
        </Scene>
      </Drawer>
    </Scene>
    {/* <Scene key='error' component={ErrorModal} /> */}
    {/* </Modal> */}
  </Router>
);

export default Scenes;

const tintColor = '#2f95dc';

export default {
  tintColor,
  tabIconDefault: '#ccc',
  tabIconSelected: tintColor,
  tabBar: '#fefefe',
  errorBackground: 'red',
  errorText: '#fff',
  warningBackground: '#EAEB5E',
  warningText: '#666804',
  noticeBackground: tintColor,
  noticeText: '#fff',

  white: '#FFFFFF',

  primary: '#1891D0',
  secondary: '#081c24',

  sec_transparent: '#0002',
  pri_transparent: '#081c24f2',
};

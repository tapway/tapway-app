import React, { Component } from 'react';
import { Provider } from 'react-redux';
import { StyleProvider } from 'native-base';
import getTheme from './theme/native-base-theme/components';
// import material from './theme/native-base-theme/variables/material';
import variables from './theme/native-base-theme/variables/variables';
import Scenes from './Scenes';
import SplashScreen from 'react-native-splash-screen';

class Setup extends Component {
  componentDidMount() {
    SplashScreen.hide();
  }
  render() {
    return (
      <StyleProvider style={getTheme(variables)}>
        <Provider store={this.props.store}>
          <Scenes />
        </Provider>
      </StyleProvider>
    );
  }
}

export default Setup;

/* eslint-disable react-native/no-inline-styles */
import React, { Component } from 'react';
import { View, Text, TouchableNativeFeedback, TextInput } from 'react-native';
import { connect } from 'react-redux';
import {
  Container,
  Header,
  Left,
  Button,
  Icon,
  Body,
  Right,
  Content,
  Grid,
  Row,
  Col,
  Card,
  Item,
  Input,
  Picker,
  Thumbnail,
  CardItem,
  H1,
  Accordion,
} from 'native-base';
import { Actions } from 'react-native-router-flux';
import styles from './styles';

const dataArray = [
  {
    title: 'Order placing questions',
    content:
      'Lorem ipsum dolor amet typewriter keytar vape meggings. Man braid pickled helvetica, Craft beer hammock selfies meditation, roof party seitan waistcoat before they sold out offal taxidermy cloud bread art party. ',
  },
  {
    title: 'Refund related questions',
    content:
      'Lorem ipsum dolor amet typewriter keytar vape meggings. Man braid pickled helvetica, Craft beer hammock selfies meditation, roof party seitan waistcoat before they sold out offal taxidermy cloud bread art party. ',
  },
  {
    title: 'Rider related questions',
    content:
      'Lorem ipsum dolor amet typewriter keytar vape meggings. Man braid pickled helvetica, Craft beer hammock selfies meditation, roof party seitan waistcoat before they sold out offal taxidermy cloud bread art party. ',
  },
  {
    title: 'Another questions category',
    content:
      'Lorem ipsum dolor amet typewriter keytar vape meggings. Man braid pickled helvetica, Craft beer hammock selfies meditation, roof party seitan waistcoat before they sold out offal taxidermy cloud bread art party. ',
  },
];

class FAQ extends Component {
  render() {
    return (
      <Container>
        <Header
          transparent
          hasTabs
          style={{
            backgroundColor: '#FFFFFF',
            borderBottomColor: '#0001',
            borderBottomWidth: 1.5,
          }}
          androidStatusBarColor={'#0001'}
          iosBarStyle="dark-content">
          <Left>
            <Button transparent onPress={() => Actions.pop()}>
              <Icon
                style={{ paddingHorizontal: 5, color: '#000000' }}
                name={'arrow-back'}
              />
            </Button>
          </Left>
          <Body>
            <Text
              style={{
                fontSize: 18,
                fontFamily: 'Nunito-SemiBold',
                color: '#030E1A',
              }}>
              FAQ
            </Text>
          </Body>
          <Right />
        </Header>
        <Content contentContainerStyle={{ padding: 20 }}>
          <View style={styles.inputContainer}>
            <H1>Frequently Asked Questions:</H1>
          </View>
          <View style={styles.inputContainer}>
            <Accordion dataArray={dataArray} expanded={0} />
          </View>
          <View style={styles.inputContainer}>
            <Button
              block
              full
              style={{
                backgroundColor: '#3FAF5D',
                borderRadius: 5,
                marginTop: 50,
              }}
              onPress={() => {}}>
              <Text
                style={{
                  color: '#ffffff',
                  fontFamily: 'Nunito-SemiBold',
                  fontSize: 14,
                }}>
                SUBMIT YOUR QUESTION
              </Text>
            </Button>
          </View>
        </Content>
      </Container>
    );
  }
}

const mapStateToProps = state => ({});

const mapDispatchToProps = {};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(FAQ);

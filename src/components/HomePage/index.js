/* eslint-disable react-native/no-inline-styles */
import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';
import { connect } from 'react-redux';
import MapView, { PROVIDER_GOOGLE, Marker } from 'react-native-maps';
import RNGooglePlaces from 'react-native-google-places';
import {
  Container,
  Header,
  Left,
  Icon,
  Body,
  Right,
  Content,
  Button,
  Card,
  Input,
} from 'native-base';
import { Actions } from 'react-native-router-flux';
import mapStyle from './mapStyle';

export class HomePage extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isPackageDetailsVisible: false,
      fromLocation: '',
      fromLocationSet: false,
      toLocation: '',
      toLocationSet: false,
      predictions: [],
      currentInput: '',
      region: {
        latitude: 6.599033,
        longitude: 3.3411348,
        latitudeDelta: 0.0922,
        longitudeDelta: 0.0421,
      },
    };
  }

  orderPickUp = () => {
    const { fromLocation, toLocation } = this.state;
    if (fromLocation.length < 0 || toLocation.length < 0) {
      alert.alert('Select location from dropdown');
    } else {
      Actions.PackageDetails({ fromLocation, toLocation });
    }
  };

  checkInput = () => {
    const { fromLocationSet, toLocationSet } = this.state;
    let inputState = true;
    if (toLocationSet && fromLocationSet) {
      inputState = false;
    }
    return inputState;
  };

  togglePackageDetails = () => {
    this.setState({
      isPackageDetailsVisible: !this.state.isPackageDetailsVisible,
    });
  };

  getPredictions = place => {
    RNGooglePlaces.getAutocompletePredictions(place, {
      //  type: 'cities',
      country: 'NG',
    })
      .then(results =>
        this.setState(
          { predictions: results },
          () => {},
          // console.log('Predictions', results),
        ),
      )
      .catch(error => console.log(error.message));
  };

  clearPredictions = () => {
    this.setState({ predictions: [] });
  };

  setLocationPlace = (place, locationType) => {
    if (locationType === 'from') {
      RNGooglePlaces.lookUpPlaceByID(place.placeID)
        .then(results => {
          console.log('logging lookUpPlaceByID', results);
          const { latitude, longitude, viewport } = results.location;
          this.setState(
            {
              fromLocationSet: true,
              fromLocation: results.name,
              fromLocation_lat: latitude,
              fromLocation_lng: longitude,
              region: {
                latitude,
                longitude: this.state.toLocation_lng
                  ? this.state.toLocation_lng
                  : longitude,
                latitudeDelta: 0.3699098778279124,
                longitudeDelta: 0.1909481361508365,
              },
            },
            () => {
              // console.log('region', JSON.stringify(region));
              this.clearPredictions();
            },
          );
        })
        .catch(error => console.log(error.message));
    } else {
      RNGooglePlaces.lookUpPlaceByID(place.placeID)
        .then(results => {
          const { latitude, longitude } = results.location;
          this.setState(
            {
              toLocationSet: true,
              toLocation: results.name,
              toLocation_lat: latitude,
              toLocation_lng: longitude,
              region: {
                latitude: this.state.fromLocation_lat
                  ? this.state.fromLocation_lat
                  : latitude,
                longitude,
                latitudeDelta: 0.3699098778279124,
                longitudeDelta: 0.1909481361508365,
              },
            },
            () => {
              this.clearPredictions();
            },
          );
        })
        .catch(error => console.log(error.message));
    }
  };

  placesLists = (place, locationType) => {
    if (locationType === 'from') {
      this.setState(
        {
          locationType,
          fromLocation: place,
          fromLocationSet: false,
          currentInput: locationType,
        },
        () => {
          this.getPredictions(place, locationType);
        },
      );
    } else {
      this.setState(
        {
          locationType,
          toLocationSet: false,
          toLocation: place,
          currentInput: locationType,
        },
        () => {
          this.getPredictions(place, locationType);
        },
      );
    }
  };

  render() {
    const {
      toLocation,
      toLocationSet,
      toLocation_lat,
      toLocation_lng,

      fromLocation,
      fromLocationSet,
      fromLocation_lat,
      fromLocation_lng,

      predictions,
      currentInput,
      region,
    } = this.state;
    return (
      <Container style={{ flex: 1, backgroundColor: '#ffffff' }}>
        <MapView
          provider={PROVIDER_GOOGLE}
          style={styles.map}
          region={region}
          // onRegionChangeComplete={res => console.log('region change', res)}
          customMapStyle={mapStyle}>
          {fromLocationSet && (
            <Marker
              key={1}
              coordinate={{
                latitude: Number(fromLocation_lat),
                longitude: Number(fromLocation_lng),
              }}
              pinColor={'#1B2E5A'}
            />
          )}
          {toLocationSet && (
            <Marker
              key={2}
              coordinate={{
                latitude: Number(toLocation_lat),
                longitude: Number(toLocation_lng),
              }}
              pinColor={'#ffb600'}
            />
          )}
        </MapView>
        <Header transparent androidStatusBarColor={'#0001'}>
          <Left>
            <Button transparent onPress={() => Actions.drawerOpen()}>
              <Icon
                style={{ paddingHorizontal: 10, color: '#000000' }}
                name={'menu'}
              />
            </Button>
          </Left>
          <Body />
          <Right>
            <View
              style={{
                backgroundColor: '#ffffff',
                shadowColor: '#000',
                shadowOffset: {
                  width: 0,
                  height: 6,
                },
                shadowOpacity: 0.37,
                shadowRadius: 7.49,

                elevation: 12,
              }}>
              <Icon
                style={{
                  paddingVertical: 8,
                  paddingHorizontal: 10,
                  color: '#3FAF5D',
                  fontSize: 18,
                }}
                type="FontAwesome"
                name={'location-arrow'}
              />
            </View>
          </Right>
        </Header>
        <View style={{ flex: 1, paddingHorizontal: 20 }}>
          <Card style={{ borderRadius: 10 }}>
            <View style={{ padding: 10 }}>
              <View style={{ flexDirection: 'row' }}>
                <View style={{ flexDirection: 'column' }}>
                  <View
                    style={{
                      flex: 1,
                      justifyContent: 'center',
                    }}>
                    <Icon
                      type="MaterialCommunityIcons"
                      name="map-marker"
                      style={{
                        color: '#1B2E5A',
                        fontSize: 18,
                      }}
                    />
                  </View>
                  <View
                    style={{
                      flex: 1,
                      justifyContent: 'center',
                    }}>
                    <Icon
                      type="MaterialCommunityIcons"
                      name="map-marker"
                      style={{
                        color: '#3FAF5D',
                        fontSize: 18,
                      }}
                    />
                  </View>
                </View>

                <View
                  style={{
                    flex: 1,
                    height: 80,
                    flexDirection: 'column',
                    marginLeft: 10,
                  }}>
                  <View
                    style={{
                      flex: 1,
                      height: 40,
                      borderBottomColor: '#E2E5E8',
                      borderBottomWidth: 1,
                    }}>
                    <Input
                      placeholder="Pickup location"
                      placeholderTextColor="#B1B1B1"
                      value={`${fromLocation}`}
                      keyboardType="default"
                      onChangeText={text => this.placesLists(text, 'from')}
                      clearButtonMode={'always'}
                      onBlur={this.clearPredictions}
                      maxLength={50}
                      // style={{ backgroundColor: , height: 40 }}
                    />
                  </View>
                  <View style={{ flex: 1, height: 40 }}>
                    <Input
                      placeholder="Dropoff location"
                      placeholderTextColor="#B1B1B1"
                      value={`${toLocation}`}
                      keyboardType="default"
                      onChangeText={text => this.placesLists(text, 'to')}
                      clearButtonMode={'always'}
                      onBlur={this.clearPredictions}
                      maxLength={50}
                      // style={{ backgroundColor: , height: 40 }}
                    />
                  </View>
                </View>
              </View>
            </View>
          </Card>

          {predictions.length > 0 && (
            <Card style={{ borderRadius: 10, marginTop: 10 }}>
              <View
                style={{
                  minheight: 50,
                  padding: 10,
                }}>
                {predictions.map(place => (
                  <TouchableOpacity
                    key={`${place.placeID}`}
                    onPress={() => this.setLocationPlace(place, currentInput)}
                    style={{
                      paddingBottom: 2,
                      // borderBottomColor: 'grey',
                      // borderBottomWidth: 0.5,
                    }}>
                    <View
                      style={{
                        flexDirection: 'row',
                        alignItems: 'center',
                      }}>
                      <Icon
                        type="MaterialCommunityIcons"
                        name="map-marker"
                        style={{
                          color: '#B1B1B1',
                          fontSize: 18,
                          padding: 5,
                        }}
                      />
                      <View
                        style={{
                          flex: 1,
                          borderBottomColor: '#E2E5E8',
                          borderBottomWidth: 1,
                          // padding: 10,
                        }}>
                        <Text style={{ fontSize: 16, padding: 10 }}>
                          {place.primaryText}
                        </Text>
                      </View>
                    </View>
                  </TouchableOpacity>
                ))}
              </View>
            </Card>
          )}
        </View>
        <View
          style={{
            flex: 1,
            position: 'absolute',
            bottom: 0,
            left: 0,
            right: 0,
            padding: 20,
          }}>
          <Button
            disabled={this.checkInput()}
            block
            full
            style={{ backgroundColor: this.checkInput() ? 'grey' : '#3FAF5D' }}
            onPress={() => this.orderPickUp()}>
            <Text
              style={{
                color: '#ffffff',
                fontFamily: 'Nunito-SemiBold',
                fontSize: 16,
              }}>
              ORDER PICKUP
            </Text>
          </Button>
        </View>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  map: {
    ...StyleSheet.absoluteFillObject,
  },
});

const mapStateToProps = state => ({});

const mapDispatchToProps = {};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(HomePage);

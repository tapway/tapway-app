import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  inputContainer: {
    flex: 1,
    marginVertical: 10,
    // backgroundColor: 'red',
  },
  btnText: {
    color: '#FFFFFF',
    fontSize: 18,
    fontFamily: 'Nunito-SemiBold',
    fontWeight: '400',
    letterSpacing: 0.5,
  },
});

export default styles;

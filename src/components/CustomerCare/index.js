/* eslint-disable react-native/no-inline-styles */
import React, { Component } from 'react';
import { View, Text, TouchableNativeFeedback, TextInput } from 'react-native';
import { connect } from 'react-redux';
import {
  Container,
  Header,
  Left,
  Button,
  Icon,
  Body,
  Right,
  Content,
  Grid,
  Row,
  Col,
  Card,
  Item,
  Input,
  Picker,
  Thumbnail,
  CardItem,
  H1,
  Accordion,
  Segment,
} from 'native-base';
import { Actions } from 'react-native-router-flux';
import styles from './styles';

class CustomerCare extends Component {
  constructor(props) {
    super(props);

    this.state = {
      profileSegment: 1,
    };
  }

  toggleSegment = num => this.setState({ profileSegment: num });

  render() {
    const { profileSegment } = this.state;
    return (
      <Container>
        <Header
          transparent
          hasTabs
          style={{
            backgroundColor: '#FFFFFF',
            borderBottomColor: '#0001',
            borderBottomWidth: 1.5,
          }}
          androidStatusBarColor={'#0001'}
          iosBarStyle="dark-content">
          <Left>
            <Button transparent onPress={() => Actions.pop()}>
              <Icon
                style={{ paddingHorizontal: 5, color: '#000000' }}
                name={'arrow-back'}
              />
            </Button>
          </Left>
          <Body>
            <Text
              style={{
                fontSize: 18,
                fontFamily: 'Nunito-SemiBold',
                color: '#030E1A',
              }}>
              Customer Care
            </Text>
          </Body>
          <Right />
        </Header>
        <Segment style={{ backgroundColor: 'transparent', marginVertical: 10 }}>
          <Button
            first
            onPress={() => this.toggleSegment(1)}
            style={{
              borderColor: 'black',
              backgroundColor: profileSegment === 1 ? '#030E1A' : '#FFFFFF',
              justifyContent: 'center',
              borderTopLeftRadius: 5,
              borderBottomLeftRadius: 5,
              width: 100,
            }}>
            <Text
              style={{ color: profileSegment === 1 ? '#FFFFFF' : '#030E1A' }}>
              Contact Us
            </Text>
          </Button>

          <Button
            onPress={() => this.toggleSegment(2)}
            style={{
              backgroundColor: profileSegment === 2 ? '#030E1A' : '#FFFFFF',
              justifyContent: 'center',
              borderColor: 'black',
              width: 100,
            }}
            last>
            <Text
              style={{ color: profileSegment === 2 ? '#FFFFFF' : '#030E1A' }}>
              Call Us
            </Text>
          </Button>

          <Button
            onPress={() => this.toggleSegment(3)}
            style={{
              backgroundColor: profileSegment === 3 ? '#030E1A' : '#FFFFFF',
              justifyContent: 'center',
              borderColor: 'black',
              borderTopRightRadius: 5,
              borderBottomRightRadius: 4,
              width: 100,
            }}
            last>
            <Text
              style={{ color: profileSegment === 3 ? '#FFFFFF' : '#030E1A' }}>
              Chat With Us
            </Text>
          </Button>
        </Segment>
        <Content contentContainerStyle={{ padding: 20 }}>
          {profileSegment === 1 && (
            <View style={{ flex: 1 }}>
              <View style={{ marginBottom: 4 }}>
                <Text style={{ color: '#030E1A', fontSize: 16 }}>
                  Create Meassage
                </Text>
              </View>
              <View style={{ marginBottom: 10 }}>
                <Card>
                  <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                    <View style={{ padding: 10 }}>
                      <Text>To:</Text>
                    </View>
                    <View style={{ flex: 1 }}>
                      <Input />
                    </View>
                  </View>
                </Card>
              </View>

              <View style={{ marginBottom: 10 }}>
                <Card>
                  <View
                    style={{
                      flexDirection: 'column',
                      padding: 10,
                      height: 200,
                    }}>
                    <View
                      style={{
                        paddingBottom: 10,
                        borderBottomColor: '#0005',
                        borderBottomWidth: 0.6,
                      }}>
                      <Text>Subject:</Text>
                    </View>
                    <View>
                      <TextInput
                        placeholder={'Write Your message here'}
                        placeholderTextColor={'#0005'}
                        multiline
                      />
                    </View>
                  </View>
                </Card>
              </View>
              <Button
                block
                primary
                style={{ backgroundColor: '#3FAF5D', marginTop: 30 }}
                onPress={() => {}}>
                <Text style={styles.btnText}>SEND MESSAGE</Text>
              </Button>
            </View>
          )}
          {profileSegment === 2 && (
            <View style={{ flex: 1 }}>
              <View style={{ marginBottom: 4 }}>
                <Text style={{ color: '#030E1A', fontSize: 16 }}>
                  Call us on any of these numbers
                </Text>
              </View>
              <Card>
                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                  <View style={{ padding: 10 }}>
                    <Icon name="call" style={{ fontSize: 16 }} />
                  </View>
                  <View style={{ flex: 1 }}>
                    <Text style={{ color: '#030E1A', fontSize: 16 }}>
                      09045678945
                    </Text>
                  </View>
                </View>
                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                  <View style={{ padding: 10 }}>
                    <Icon name="call" style={{ fontSize: 16 }} />
                  </View>
                  <View style={{ flex: 1 }}>
                    <Text style={{ color: '#030E1A', fontSize: 16 }}>
                      09045678945
                    </Text>
                  </View>
                </View>
              </Card>
            </View>
          )}
          {profileSegment === 3 && (
            <View style={{ flex: 1 }}>
              <View style={{ marginBottom: 4 }}>
                <Text style={{ color: '#030E1A', fontSize: 16 }}>
                  Chat with us on whatsapp
                </Text>
              </View>
              <Card>
                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                  <View style={{ padding: 10 }}>
                    <Icon
                      type={'Ionicons'}
                      name="logo-whatsapp"
                      style={{ fontSize: 16, color: '#4CAF50' }}
                    />
                  </View>
                  <View style={{ flex: 1 }}>
                    <Text style={{ color: '#030E1A', fontSize: 16 }}>
                      09045678945
                    </Text>
                  </View>
                </View>
                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                  <View style={{ padding: 10 }}>
                    <Icon
                      type={'Ionicons'}
                      name="logo-whatsapp"
                      style={{ fontSize: 16, color: '#4CAF50' }}
                    />
                  </View>
                  <View style={{ flex: 1 }}>
                    <Text style={{ color: '#030E1A', fontSize: 16 }}>
                      09045678945
                    </Text>
                  </View>
                </View>
              </Card>
            </View>
          )}
        </Content>
      </Container>
    );
  }
}

const mapStateToProps = state => ({});

const mapDispatchToProps = {};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(CustomerCare);

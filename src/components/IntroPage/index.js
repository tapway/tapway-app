/* eslint-disable react-native/no-inline-styles */
import React, {Component} from 'react';
import {StyleSheet, View, Text} from 'react-native';
import {connect} from 'react-redux';
import Image from 'react-native-remote-svg';
import AppIntroSlider from 'react-native-app-intro-slider';
import {Actions} from 'react-native-router-flux';
import {Header} from 'native-base';

const slides = [
  {
    key: 'page1',
    title: 'Welcome to Tapway',
    text:
      'Now you can make quick and affordable deliveries at the tap of a button',
    image: require('../../assets/icons/on_the_way.svg'),
    backgroundColor: '#348349',
  },
  {
    key: 'page2',
    title: 'Request a Rider',
    text:
      'Enter your pickup location, destination, item name and quantity and tap on proceed to view delivery price.',
    image: require('../../assets/icons/request.svg'),
    backgroundColor: '#74AA50',
  },
  {
    key: 'page3',
    title: 'Get Paired',
    text:
      'Get paired with a rider then make payment to view rider’s details and ETA.',
    image: require('../../assets/icons/Group_4.svg'),
    backgroundColor: '#74AA50',
  },
  {
    key: 'page4',
    title: 'Relax',
    text:
      'This is a secured way to getting your item delivered hassle free, once your item is delivered you can rate your rider experience',
    image: require('../../assets/icons/Group_5.svg'),
    backgroundColor: '#1891D0',
  },
];

class IntroPage extends Component {
  constructor(props) {
    super(props);

    this.state = {
      showRealApp: false,
    };
  }

  _renderItem = ({item}) => {
    return (
      <View style={[styles.slide, {backgroundColor: item.backgroundColor}]}>
        <Header
          transparent
          androidStatusBarColor={'transparent'}
          iosBarStyle={'light-content'}
        />
        <Image source={item.image} style={styles.image} />
        <Text style={styles.title}>{item.title}</Text>
        <Text style={styles.text}>{item.text}</Text>
      </View>
    );
  };
  _onDone = () => {
    // User finished the introduction. Show real app through
    // navigation or simply by controlling state
    this.setState({showRealApp: true}, () => Actions.Login());
  };

  render() {
    return (
      <AppIntroSlider
        showSkipButton={true}
        renderItem={this._renderItem}
        slides={slides}
        onDone={this._onDone}
        onSkip={this._onDone}
      />
    );
  }
}

const styles = StyleSheet.create({
  slide: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#9DD6EB',
    padding: 15,
  },
  title: {
    fontFamily: 'Nunito-SemiBold',
    color: '#FFFFFF',
    fontSize: 25,
    textAlign: 'center',
    marginTop: 20,
    marginVertical: 10,
  },
  // image: {
  //   marginBottom: 30,
  // },
  text: {
    fontFamily: 'Nunito-Regular',
    color: '#FFFFFF',
    fontSize: 18,
    opacity: 0.7,
    textAlign: 'center',
    marginVertical: 10,
  },
});

const mapStateToProps = state => ({});

const mapDispatchToProps = {};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(IntroPage);

/* eslint-disable react-native/no-inline-styles */
import React, { Component } from 'react';
import { View, Text } from 'react-native';
import { connect } from 'react-redux';
import styles from './styles';
import {
  Container,
  Header,
  Left,
  Icon,
  Body,
  Right,
  Content,
  Card,
  CardItem,
  Input,
  Button,
  Item,
} from 'native-base';
import { Actions } from 'react-native-router-flux';
import Modal from 'react-native-modal';

export class ForgetPassword extends Component {
  constructor(props) {
    super(props);

    this.state = {
      emailIsReset: false,
    };
  }

  render() {
    const { emailIsReset } = this.state;
    return (
      <Container style={styles.container}>
        <Header transparent androidStatusBarColor={'#0003'}>
          <Left style={{ paddingHorizontal: 5 }}>
            <Button transparent onPress={() => Actions.Login()}>
              <Icon name={'close'} style={styles.closeIcon} />
            </Button>
          </Left>
          <Body />
          <Right />
        </Header>
        <Content>
          <Card transparent>
            <CardItem>
              <Text style={styles.title}>Forgot Password</Text>
            </CardItem>
            <CardItem>
              <Text style={styles.subTitle}>
                Please provide your Tapway registered email address
              </Text>
            </CardItem>
            <CardItem>
              <Item regular style={styles.emailContainer}>
                <Input
                  placeholder={'Email address'}
                  placeholderTextColor={'#0309'}
                  style={styles.emailInput}
                />
              </Item>
            </CardItem>

            <View style={{ padding: 16 }}>
              <Button
                block
                full
                onPress={() => this.setState({ emailIsReset: true })}
                style={styles.submitButton}>
                <Text style={styles.submitText}>RESET MY PASSWORD</Text>
              </Button>
            </View>
          </Card>
        </Content>
        <Modal
          isVisible={emailIsReset}
          coverScreen={true}
          style={{ backgroundColor: '#FFFFFF' }}
          backdropColor={'#FFFFFF'}>
          <View style={styles.modalContainer}>
            <View style={{ alignSelf: 'center' }}>
              <Icon name={'checkmark-circle'} style={styles.modalIcon} />
            </View>
            <Card transparent>
              <CardItem style={{ justifyContent: 'center' }}>
                <Text style={styles.modalTitle}>
                  Password Recovered Successful
                </Text>
              </CardItem>

              <CardItem style={{ justifyContent: 'center' }}>
                <Text style={styles.modalSubtitle}>
                  {'Your Password was resetted succesfully,\n Please Login'}
                </Text>
              </CardItem>

              <View style={{ padding: 16 }}>
                <Button
                  block
                  full
                  onPress={() =>
                    this.setState({ emailIsReset: false }, () => {
                      Actions.Login();
                    })
                  }
                  style={styles.modalButton}>
                  <Text style={styles.modalButtonText}>LOGIN</Text>
                </Button>
              </View>
            </Card>
          </View>
        </Modal>
      </Container>
    );
  }
}

const mapStateToProps = state => ({});

const mapDispatchToProps = {};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ForgetPassword);

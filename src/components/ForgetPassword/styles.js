import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  container: { flex: 1, backgroundColor: '#FFFFFF' },
  closeIcon: { fontSize: 25, color: '#030E1A' },
  title: {
    fontFamily: 'Nunito-Bold',
    color: '#030E1A',
    fontSize: 30,
  },
  subTitle: {
    fontSize: 18,
    fontFamily: 'Nunito-Regular',
  },
  emailContainer: {
    borderColor: '#EFF4F5',
    borderRadius: 10,
  },
  emailInput: {
    backgroundColor: '#EFF4F5',
    paddingHorizontal: 10,
    borderRadius: 5,
  },
  submitButton: {
    backgroundColor: '#3FAF5D',
    borderRadius: 5,
  },
  submitText: {
    color: '#FFFFFF',
    fontSize: 16,
    letterSpacing: 0.2,
  },
  modalContainer: {
    flex: 1,
    justifyContent: 'center',
    alignContent: 'center',
  },
  modalIcon: { fontSize: 180, color: '#1B2E5A' },
  modalTitle: {
    textAlign: 'center',
    color: '#030E1A',
    fontSize: 25,
    fontFamily: 'Nunito-Bold',
  },
  modalSubtitle: {
    textAlign: 'center',
    color: '#858F99',
    fontSize: 16,
  },
  modalButton: {
    backgroundColor: '#3FAF5D',
    borderRadius: 5,
    padding: 50,
  },
  modalButtonText: {
    color: '#FFFFFF',
    fontSize: 16,
    letterSpacing: 0.2,
  },
});

export default styles;

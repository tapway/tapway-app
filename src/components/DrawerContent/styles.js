import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  listText: {
    color: '#030E1A',
    fontFamily: 'Nunito-SemiBold',
    fontSize: 18,
  },
  listIcon: {
    color: '#1B2E5A',
    fontFamily: 'Nunito-SemiBold',
  },
});

export default styles;

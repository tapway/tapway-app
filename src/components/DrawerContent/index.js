/* eslint-disable react-native/no-inline-styles */
import React, { Component } from 'react';
import { View, Text, Alert, TouchableOpacity } from 'react-native';
import { connect } from 'react-redux';
import {
  Container,
  Content,
  Header,
  Icon,
  Left,
  Body,
  Right,
  Card,
  Thumbnail,
  Button,
  CardItem,
} from 'native-base';
import { Actions } from 'react-native-router-flux';
import styles from './styles';

export class DrawerContent extends Component {
  render() {
    return (
      <Container style={{ backgroundColor: '#1B2E5A' }}>
        <Header transparent androidStatusBarColor="transparent">
          <Left>
            <Button transparent onPress={() => Actions.drawerClose()}>
              <Icon name="close" style={{ color: '#FFFF' }} />
            </Button>
          </Left>
          <Body />
          <Right />
        </Header>
        <Card transparent>
          <View style={{ flexDirection: 'row', padding: 16 }}>
            <Left
              style={{
                flexDirection: 'column',
                alignContent: 'flex-start',
                alignItems: 'flex-start',
              }}>
              <View
                style={{
                  backgroundColor: '#ffff',
                  borderRadius: 5,
                  padding: 2,
                  shadowColor: '#0000',
                  shadowOffset: {
                    width: 0,
                    height: 2,
                  },
                  shadowOpacity: 0.23,
                  shadowRadius: 2.0,
                  elevation: 4,
                }}>
                <Thumbnail
                  style={{ borderRadius: 5, height: 50, width: 50 }}
                  square
                  large
                  source={require('../../assets/icons/man.png')}
                />
              </View>
              <View
                style={{
                  justifyContent: 'flex-end',
                  alignContent: 'flex-end',
                }}>
                <Text
                  style={{
                    color: '#ffffff',
                    fontFamily: 'Nunito-SemiBold',
                    fontSize: 20,
                    marginVertical: 10,
                  }}>
                  Williams Adewale
                </Text>
                <TouchableOpacity
                  onPress={() => {
                    Actions.drawerClose();
                    Actions.ProfileSetting();
                  }}>
                  <Text
                    style={{
                      color: '#ffffff',
                      opacity: 0.8,
                      fontSize: 15,
                    }}>
                    View Profile
                  </Text>
                </TouchableOpacity>
              </View>
            </Left>
          </View>
        </Card>
        <Content style={{ backgroundColor: '#FFFF' }}>
          <Card transparent>
            <TouchableOpacity onPress={() => Actions.HomePage()}>
              <CardItem>
                <Icon name="home" style={styles.listIcon} />
                <Text style={styles.listText}>Home</Text>
              </CardItem>
            </TouchableOpacity>

            <TouchableOpacity onPress={() => Actions.OrderHistory()}>
              <CardItem>
                <Icon
                  type="MaterialCommunityIcons"
                  name="restore-clock"
                  style={styles.listIcon}
                />
                <Text style={styles.listText}>Order History</Text>
              </CardItem>
            </TouchableOpacity>

            <TouchableOpacity onPress={() => Actions.TrackOrder()}>
              <CardItem>
                <Icon
                  type="MaterialCommunityIcons"
                  name="map-marker-path"
                  style={[styles.listIcon, { marginRight: 6, marginLeft: -10 }]}
                />
                <Text style={[styles.listText, { paddingLeft: 5 }]}>
                  Track Order
                </Text>
              </CardItem>
            </TouchableOpacity>

            <TouchableOpacity onPress={() => Actions.Wallet()}>
              <CardItem>
                <Icon name="wallet" style={styles.listIcon} />
                <Text style={styles.listText}>Wallet</Text>
              </CardItem>
            </TouchableOpacity>

            <TouchableOpacity onPress={() => Actions.FAQ()}>
              <CardItem>
                <Icon
                  type="MaterialCommunityIcons"
                  name="comment-question"
                  style={styles.listIcon}
                />
                <Text style={styles.listText}>FAQs</Text>
              </CardItem>
            </TouchableOpacity>

            <TouchableOpacity onPress={() => Actions.CustomerCare()}>
              <CardItem>
                <Icon
                  type="AntDesign"
                  name="customerservice"
                  style={styles.listIcon}
                />
                <Text style={styles.listText}>Customer Care</Text>
              </CardItem>
            </TouchableOpacity>

            <CardItem />
            <TouchableOpacity
              onPress={() => {
                Alert.alert('Are you sure ?', 'You want to logout', [
                  {
                    text: 'No',
                    onPress: () => console.log('Cancel Pressed'),
                    style: 'cancel',
                  },
                  { text: 'Yes', onPress: () => Actions.Login() },
                ]);
              }}>
              <CardItem onPress={() => Actions.Login()}>
                <Icon
                  type="MaterialCommunityIcons"
                  name="logout"
                  style={styles.listIcon}
                />
                <Text style={styles.listText}>Logout</Text>
              </CardItem>
            </TouchableOpacity>
          </Card>
        </Content>
      </Container>
    );
  }
}

const mapStateToProps = state => ({});

const mapDispatchToProps = {};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(DrawerContent);

/* eslint-disable react-native/no-inline-styles */
import React, { Component } from 'react';
import { View, Text } from 'react-native';
import { connect } from 'react-redux';

import {
  Container,
  Header,
  Left,
  Body,
  Right,
  Button,
  Icon,
  Segment,
  Content,
  Thumbnail,
  Item,
  Input,
  Card,
} from 'native-base';
import styles from './styles';
import { Actions } from 'react-native-router-flux';

export class ProfileSetting extends Component {
  constructor(props) {
    super(props);

    this.state = {
      profileSegment: 1,
    };
  }

  toggleSegment = num => this.setState({ profileSegment: num });

  render() {
    const { profileSegment } = this.state;
    return (
      <Container>
        <Header
          transparent
          hasTabs
          style={{
            backgroundColor: '#FFFFFF',
            borderBottomColor: '#0001',
            borderBottomWidth: 1.5,
          }}
          androidStatusBarColor={'#0001'}
          iosBarStyle="dark-content">
          <Left>
            <Button transparent onPress={() => Actions.drawerOpen()}>
              <Icon
                style={{
                  paddingHorizontal: 10,
                  color: '#000000',
                }}
                name={'menu'}
              />
            </Button>
          </Left>
          <Body>
            <Text
              style={{
                fontSize: 18,
                color: '#030E1A',
                fontFamily: 'Nunito-SemiBold',
              }}>
              Profile Setting
            </Text>
          </Body>
          <Right />
        </Header>
        <Segment style={{ backgroundColor: 'transparent', marginVertical: 10 }}>
          <Button
            first
            onPress={() => this.toggleSegment(1)}
            style={{
              borderColor: 'black',
              backgroundColor: profileSegment === 1 ? '#030E1A' : '#FFFFFF',
              justifyContent: 'center',
              borderTopLeftRadius: 5,
              borderBottomLeftRadius: 5,
              width: 150,
            }}>
            <Text
              style={{ color: profileSegment === 1 ? '#FFFFFF' : '#030E1A' }}>
              Profile
            </Text>
          </Button>
          <Button
            onPress={() => this.toggleSegment(2)}
            style={{
              backgroundColor: profileSegment === 2 ? '#030E1A' : '#FFFFFF',
              justifyContent: 'center',
              borderColor: 'black',
              borderTopRightRadius: 5,
              borderBottomRightRadius: 4,
              width: 150,
            }}
            last>
            <Text
              style={{ color: profileSegment === 2 ? '#FFFFFF' : '#030E1A' }}>
              Security Settings
            </Text>
          </Button>
        </Segment>
        <Content padder contentContainerStyle={{ padding: 16 }}>
          {(profileSegment === 1 && (
            <View style={{ flex: 1 }}>
              <View
                style={{
                  alignItems: 'center',
                }}>
                <View>
                  <Thumbnail
                    square
                    large
                    source={require('../../assets/icons/man.png')}
                  />
                  <View
                    style={{
                      position: 'absolute',
                      right: -10,
                      bottom: -10,
                      borderRadius: 30,
                      borderColor: '#FFFFFF',
                      borderWidth: 1,
                      backgroundColor: '#1B2E5A',
                    }}>
                    <Icon
                      type={'AntDesign'}
                      name={'edit'}
                      style={{ color: '#FFFFFF', fontSize: 16, padding: 5 }}
                    />
                  </View>
                </View>
              </View>
              <View style={{ marginVertical: 30 }}>
                <View style={{ flexDirection: 'row' }}>
                  <View style={[styles.inputContainer, { marginRight: 2 }]}>
                    <Text style={styles.inputLabel}>First Name</Text>
                    <Item regular style={styles.inputItem}>
                      <Input
                        placeholder="Williams"
                        placeholderTextColor={'#383838'}
                        style={styles.inputText}
                      />
                    </Item>
                  </View>
                  <View style={[styles.inputContainer, { marginLeft: 2 }]}>
                    <Text style={styles.inputLabel}>Last Name</Text>
                    <Item regular style={styles.inputItem}>
                      <Input
                        placeholder="Adebowale"
                        placeholderTextColor={'#383838'}
                        style={styles.inputText}
                      />
                    </Item>
                  </View>
                </View>

                <View style={styles.inputContainer}>
                  <Text style={styles.inputLabel}>Phone number</Text>
                  <Item regular style={styles.inputItem}>
                    <Input
                      placeholder="07940480484"
                      placeholderTextColor={'#383838'}
                      style={styles.inputText}
                    />
                  </Item>
                </View>
                <View style={styles.inputContainer}>
                  <Text style={styles.inputLabel}>Home Address</Text>
                  <Item regular style={styles.inputItem}>
                    <Input
                      placeholder="5 Akilo Street lady lagos."
                      placeholderTextColor={'#383838'}
                      style={styles.inputText}
                    />
                  </Item>
                </View>
                <View style={{ marginVertical: 30 }}>
                  <Button block style={{ backgroundColor: '#B6B6B6' }}>
                    <Text
                      style={{
                        color: '#FFFFFF',
                        fontFamily: 'Nunito-SemiBold',
                      }}>
                      UPDATE
                    </Text>
                  </Button>
                </View>
              </View>
            </View>
          )) || (
            <View>
              <View>
                <Card
                  transparent
                  style={{ backgroundColor: '#F1F2F5', padding: 30 }}>
                  <Text style={{ color: '#858585' }}>
                    Your verification information helps us secure your account.
                  </Text>
                </Card>
              </View>

              <View style={{ justifyContent: 'center', marginVertical: 20 }}>
                <View style={styles.inputContainer}>
                  <Text style={styles.inputLabel}>Email ID</Text>
                  <Item regular style={styles.inputItem}>
                    <Input
                      placeholder="willywale@gmail.com"
                      placeholderTextColor={'#383838'}
                      style={styles.inputText}
                    />
                  </Item>
                </View>
                <View style={styles.inputContainer}>
                  <Text style={styles.inputLabel}>Password</Text>
                  <Item regular style={styles.inputItem}>
                    <Input
                      placeholder="password"
                      placeholderTextColor={'#383838'}
                      secureTextEntry={true}
                      style={styles.inputText}
                    />
                  </Item>
                </View>
              </View>

              <View style={{ marginVertical: 30 }}>
                <Button block style={{ backgroundColor: '#B6B6B6' }}>
                  <Text
                    style={{
                      color: '#FFFFFF',
                      fontFamily: 'Nunito-SemiBold',
                    }}>
                    UPDATE
                  </Text>
                </Button>
              </View>
            </View>
          )}
        </Content>
      </Container>
    );
  }
}

const mapStateToProps = state => ({});

const mapDispatchToProps = {};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ProfileSetting);
